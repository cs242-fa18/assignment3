import React from 'react';
import axios from 'axios';
import { AsyncStorage } from 'react-native';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import { List, ListItem, Button, SearchBar } from 'react-native-elements';
import { WebBrowser } from 'expo';


export default class FollowingScreen extends React.Component {
  static navigationOptions = {
    title: 'Following',
  };

  constructor(props){
    super(props);

    this.state = {
      data: [],
      pageNum: 1,
      loading: false,
      baseUser: 'cs242HW',
      password: 'assignment3',
      following: {},
      bufferData: [],
      query: '',
    };
    this.makeRequest = this.makeRequest.bind(this);
  }

  componentDidMount(){
    this.makeRequest(this.state.baseUser);
  }

  _storeData = async (key, item) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(item));
    } catch (e) {
      console.log(e.message);
    }
  }

  _retrieveData = async (key) => {
    try {
      const data = await AsyncStorage.getItem(key+'Following');
      console.log("following key: "+key+'Following');
      console.log("following data: "+data);
      if (data){
        let following = JSON.parse(data);
        var following_dict = [];
        following.forEach(function(item) {
          following_dict.push({
            key: item.name,
            value: true,
          });
        }.bind(this));

        this.setState({
          data: following,
          bufferData: following,
          loading: false,
          following: following_dict,
        });
      } else {
        axios.get('https://api.github.com/users/'+key+'/following?page='+this.state.pageNum)
          .then((res) => {
            if (res.length != 0){
              var data = []
              for (var i=0; i<res.data.length; i++){
                const id = res.data[i].id;
                const name = res.data[i].login;
                const avatar_url = res.data[i].avatar_url;
                const html_url = res.data[i].html_url;
                data.push({id: id, name: name, avatar: avatar_url, url: html_url})
              }
              let following = data;
              var following_dict = [];
              following.forEach(function(item) {
                following_dict.push({
                  key: item.name,
                  value: true,
                });
              }.bind(this));

              this.setState({
                data: following,
                bufferData: following,
                loading: false,
                following: following_dict,
              });
            }
          })
          .catch(err => {
            console.log(err);
          })
      }
    } catch (e) {
      console.log(e.message);
    }
  }

  makeRequest = (userName) => {
    this.setState({
      loading: true,
    })

    this._retrieveData(userName);
  }

  _handlePress = () => {
    axios.get('https://api.github.com/users/'+this.state.baseUser+'/following?page='+this.state.pageNum)
      .then((res) => {
        console.log("Refresh triggered!");
        if (res.length != 0){
          var data = []
          for (var i=0; i<res.data.length; i++){
            const id = res.data[i].id;
            const name = res.data[i].login;
            const avatar_url = res.data[i].avatar_url;
            const html_url = res.data[i].html_url;
            data.push({id: id, name: name, avatar: avatar_url, url: html_url})
          }
          this.setState({
            data: data,
            bufferData: data,
            loading: false,
          });
          this._storeData(this.state.baseUser+'Following', data);
        }
      })
      .catch(err => {
        console.log(err);
      })
  }

  _changeFollow = (key) => {
    const following = this.state.following[key];
    if (following){
      fetch('https://api.github.com/user/starred/'+this.state.baseUser+'/'+key+'?access_token=bbda13b781bd3e04037b811c46bbcbfd759fa9d6', {
        method: 'DELETE'
      })
        .then((res) => {
            var dict = this.state.following;
            dict[key] = false;
            this.setState({
              following: dict,
            });
        })
        .catch(e => {
          console.log(e);
        })
    } else {
      fetch('https://api.github.com/user/starred/'+this.state.baseUser+'/'+key+'?access_token=bbda13b781bd3e04037b811c46bbcbfd759fa9d6', {
        method: 'PUT'
      })
        .then((res) => {
          var dict = this.state.following;
          dict[key] = true;
          this.setState({
            following: dict,
          });
        })
        .catch(e => {
          console.log(e.message);
        })
    }
  }

  _handleQueryChange = query => {
    this.setState(state => ({ ...state, query: query || "" }), () => {
      const query = this.state.query;
      if (query){
        let tempData = [];
        for (var i=0; i<this.state.bufferData.length; i++){
          if (this.state.bufferData[i].name.toLowerCase().includes(query.toLowerCase())){
            tempData.push(this.state.bufferData[i]);
          }
        }
        this.setState({
          data: tempData,
        });
      } else {
        this.setState({
          data: this.state.bufferData,
        });
      }
    });
  }

  _handleSearchClear = () => this._handleQueryChange("");

  _handleSearchCancel = () => this._handleQueryChange("");

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%",
        }}
      />
    );
  };

  render() {
    return (
      <View>
        <SearchBar
          lightTheme
          platform='ios'
          placeholder='Search for a repo ...'
          onChangeText={this._handleQueryChange.bind(this)}
          onClear={this._handleSearchClear.bind(this)}
          onCancel={this._handleSearchCancel.bind(this)}
          value={this.state.query}
        />
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem
              roundAvatar
              leftAvatar={{source: {uri: item.avatar}}}
              title={item.name}
              titleStyle={{fontWeight: 'bold'}}
              underlayColor='#E5E8E8'
              containerStyle={{ borderBottomWidth: 0 }}
              onPress={() => this.props.navigation.navigate('Profile', { name: item.name })}
              rightIcon = {<Button
                            title={this.state.following[item.name]?'follow':'unfollow'}
                            onPress={this._changeFollow.bind(this, item.name)}
                            buttonStyle={{
                              backgroundColor: 'rgba(195, 155, 211, 1)',
                              borderColor: 'transparent',
                              borderWidth: 0,
                              alignSelf: 'center',
                            }}
                            />
                          }
            />
          )}
          keyExtractor={item => item.id.toString()}
          ListFooterComponent={this.renderFooter}
          ItemSeparatorComponent={this.renderSeparator}
        />
        <Button
          title={'REFRESH'}
          onPress={this._handlePress.bind(this)}
          buttonStyle={{
            backgroundColor: 'rgba(195, 155, 211, 1)',
            borderColor: 'transparent',
            borderWidth: 0,
            alignSelf: 'center',
            alignItems: 'center',
            width: '40%',
          }}
          containerStyle={{
            marginTop: '5%',
          }}
        />
      </View>
    );
  }
}
