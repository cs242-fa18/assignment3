import React from 'react';
import axios from 'axios';
import { AsyncStorage } from 'react-native';
import { WebBrowser } from 'expo';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet,
  Alert,
  Button,
  TouchableOpacity,
} from 'react-native';

import Header from './Header';
import Intro from './Intro';
import Detail from './Detail';

export default class ProfileScreen extends React.Component {

  static navigationOptions = {
    title: 'Profile',
    headerRight: (
      <TouchableOpacity onPress={()=> { WebBrowser.openBrowserAsync('https://github.com/notifications') }} style={{paddingRight: 20}}>
        <Image
          source={{uri: "https://cdn4.iconfinder.com/data/icons/basic-ui-elements/700/09_bell-512.png"}}
          style={{width:30, height: 30}}
        />
      </TouchableOpacity>
    ),
  }

  constructor(props){
    super(props);
    this.state = {
      data: {
        profilePic: '',
        name: '',
        userName: '',
        createdAt: '',
        bio: '',
        website: '',
        email: 'cs242HW@info.com',
        repo: -1,
        followers: -1,
        following: -1,
      },
      baseUser: 'cs242HW',
    }
  }

  componentDidMount(){
    this._retrieveData(this.state.baseUser);
  }

  _storeData = async (key, item) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(item));
    } catch (e) {
      console.log(e.message);
    }
  }

  _retrieveData = async (key) => {
    try {
      const data = await AsyncStorage.getItem(key);
      console.log("db key: "+key);
      console.log("db data: "+data);
      if (data){
        this.setState({
          data: JSON.parse(data),
        });
      } else {
        console.log("first time fetching data !");
        // First time fetching user data
        axios.get('https://api.github.com/users/'+key)
          .then((res) => {
            this.setState({
              data: {
                profilePic: res.data.avatar_url,
                name: res.data.name,
                userName: res.data.login,
                createdAt: res.data.created_at,
                bio: res.data.bio,
                website: res.data.html_url,
                repo: res.data.public_repos,
                followers: res.data.followers,
                following: res.data.following,
              }
            }, () => {
              this._storeData(key, this.state.data);
            });
          })
          .catch(err => {
            console.log(err);
            Alert.alert("Error occured when making API call!");
          })

        // First time fetching user repo data
        axios.get('https://api.github.com/users/'+key+'/repos')
          .then((res) => {
            if (res.length != 0){
              var data = []
              for (var i=0; i<res.data.length; i++){
                const id = res.data[i].id;
                const name = res.data[i].name;
                const owner = res.data[i].owner.login;
                const description = res.data[i].description;
                const url = res.data[i].html_url;
                data.push({id: id, name: name, owner: owner, description: description, url: url})
              }
              this._storeData(key+'Repos', data);
            }
          })
          .catch(err => {
            console.log(err);
            Alert.alert("Error occured when making API call!");
          })

        axios.get('https://api.github.com/users/'+key+'/followers')
          .then((res) => {
            if (res.length != 0){
              var data = []
              for (var i=0; i<res.data.length; i++){
                const id = res.data[i].id;
                const name = res.data[i].login;
                const avatar_url = res.data[i].avatar_url;
                const html_url = res.data[i].html_url;
                const repo = res.data[i].repos_url;
                const followers = res.data[i].followers_url;
                const following = res.data[i].following_url;
                data.push({id: id, name: name, avatar: avatar_url, url: html_url, repo: repo, followers: followers, following: following});
              }
              this._storeData(key+'Followers', data);
            }
          })
          .catch(err => {
            console.log(err);
          })

        axios.get('https://api.github.com/users/'+key+'/following')
          .then((res) => {
            if (res.length != 0){
              var data = []
              for (var i=0; i<res.data.length; i++){
                const id = res.data[i].id;
                const name = res.data[i].login;
                const avatar_url = res.data[i].avatar_url;
                const html_url = res.data[i].html_url;
                data.push({id: id, name: name, avatar: avatar_url, url: html_url})
              }
              this._storeData(key+'Following', data);
            }
          })
          .catch(err => {
            console.log(err);
          })
      }
    } catch (e) {
      console.log(e.message);
    }
  }

  componentWillReceiveProps(nextProps){
    const userName = nextProps.navigation.getParam('name');
    console.log('Received name: '+userName);
    this._retrieveData(userName);
  }

  _handleRefresh = () => {
    axios.get('https://api.github.com/users/'+this.state.baseUser)
      .then((res) => {
        this.setState({
          data: {
            profilePic: res.data.avatar_url,
            name: res.data.name,
            userName: res.data.login,
            createdAt: res.data.created_at,
            bio: res.data.bio,
            website: res.data.html_url,
            repo: res.data.public_repos,
            followers: res.data.followers,
            following: res.data.following,
          }
        }, () => {
          this._storeData(this.state.baseUser, this.state.data);
        });
      })
      .catch(err => {
        console.log(err);
        Alert.alert("Error occured when making API call!");
      })
  }

  render(){
    return(
      <ScrollView style={styles.profileContainer}>
        <Header
          profilePic={this.state.data.profilePic}
          name={this.state.data.name}
          userName={this.state.data.userName}
          createdAt={this.state.data.createdAt}
        />
        <Intro
          bio={this.state.data.bio}
          website={this.state.data.website}
          email={this.state.data.email}
        />
        <Detail
          baseUser={this.state.baseUser}
          userName={this.state.userName}
          repo={this.state.data.repo}
          followers={this.state.data.followers}
          following={this.state.data.following}
          navigation={this.props.navigation}
        />
        <Button
          onPress={this._handleRefresh.bind(this)}
          title="REFRESH"
          color="#CCD1D1"
          accessibilityLabel="Update My Profile Page"
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    paddingHorizontal: '5%',
  },
});
