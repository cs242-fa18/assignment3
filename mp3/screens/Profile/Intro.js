import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { WebBrowser } from 'expo';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

export default class Intro extends Component {
  render(){
    const { bio, website, email } = this.props;

    if (website != ''){
      return(
        <View style={styles.intro}>
          <View style={styles.infoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.infoTitle}>Bio</Text>
            </View>
            <View style={styles.textContainer}>
              <Text>{this.props.bio}</Text>
            </View>
          </View>
          <View style={styles.infoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.infoTitle}>Website</Text>
            </View>
            <View style={styles.textContainer}>
              <TouchableOpacity onPress={() => { WebBrowser.openBrowserAsync(this.props.website);}}>
                <Text style={styles.infoText}>{this.props.website}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.infoContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.infoTitle}>Email</Text>
            </View>
            <View style={styles.textContainer}>
              <Text>{this.props.email}</Text>
            </View>
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  intro: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-evenly',
    borderBottomColor: '#E5E8E8',
    borderBottomWidth: 1,
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {
    flex: 1,
    alignItems: 'baseline',
    paddingVertical: 20,
  },
  textContainer: {
    flex: 1,
    alignItems: 'baseline',
    paddingVertical: 20,
    paddingHorizontal: '5%',
  },
  infoTitle: {
    fontWeight: 'bold',
    paddingHorizontal: 50,
  },
  infoText: {
    color: 'blue',
  }
});
