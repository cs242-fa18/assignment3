import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';


export default class Header extends Component {

  render(){
    if (this.props.profilePic != ''){
      return(
        <View style={styles.headerBackground}>
          <View style={styles.header}>
            <View style={styles.profilePicContainer}>
              <Image style={styles.profilePic} source={{uri: this.props.profilePic}}/>
            </View>
            <Text style={styles.name}>{this.props.name}</Text>
            <Text style={styles.name}>{'@'+this.props.userName}</Text>
            <Text style={styles.name}>{'Created At: '+this.props.createdAt}</Text>
          </View>
      </View>
      );
    } else {
      return null;
    }
  }
}

Header.propTypes = {
  profilePic: PropTypes.string.isRequired,
  // name: PropTypes.string.isRequired, // This is weird, but some people just don't "have" a name.
  userName: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
}

const styles = StyleSheet.create({
  headerBackground: {
    flex: 1,
    width: null,
    alignSelf: 'stretch',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderBottomColor: '#E5E8E8',
    borderBottomWidth: 1,
    padding: 20,
  },
  profilePicContainer: {
    width: 100,
    height: 100,
  },
  profilePic: {
    flex: 1,
    width: null,
    alignSelf: 'stretch',
  },
  name: {
    marginTop: '5%',
    fontSize: 14,
    fontWeight: 'bold',
  },
});
