import React, { Component } from 'react';
import { Button } from 'react-native-elements';
import PropTypes from 'prop-types';
import { AsyncStorage } from 'react-native';
import {
  View,
  Text,
  StyleSheet,
  Alert,
} from 'react-native';

export default class Detail extends Component {

  _handlePress = (name) => {
    this.props.navigation.navigate('Profile', {name: name})
  }

  render(){
    const { baseUser, userName, repo, followers, following, navigation } = this.props;

    if (repo != -1){
      return(
        <View style={styles.detail}>
          <View style={styles.topContainer}>
            <Button
              title={'REPO #'+(repo == null ? 0:repo)}
              onPress={() => {navigation.navigate('Repo');}}
              buttonStyle={{
                backgroundColor: 'rgba(231, 76, 60, 1)',
                borderColor: 'transparent',
                borderWidth: 0,
                marginBottom: '5%',
              }}
            />
          </View>
          <View style={styles.middleContainer}>
            <Button
              title={'FOLLOWERS\n'+'# '+(followers == null ? 0:followers)}
              onPress={() => {navigation.navigate('Follower')}}
              buttonStyle={{
                backgroundColor: 'rgba(247, 220, 111, 1)',
                borderColor: 'transparent',
                borderWidth: 0,
                marginRight: '5%',
              }}
            />
            <Button
              title={'FOLLOWING\n'+'# '+(following == null ? 0:following)}
              onPress={() => {navigation.navigate('Following')}}
              buttonStyle={{
                backgroundColor: 'rgba(88, 214, 141, 1)',
                borderColor: 'transparent',
                borderWidth: 0,
                marginRight: '5%',
              }}
            />
          </View>
          <View style={styles.bottomContainer}>
            <Button
              title={'BACK TO ME'}
              onPress={this._handlePress.bind(this, baseUser)}
              buttonStyle={{
                backgroundColor: 'rgba(93, 173, 226, 1)',
                borderColor: 'transparent',
                borderWidth: 0,
              }}
            />
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  detail: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: '5%',
  },
  topContainer: {
    alignItems: 'center',
    paddingTop: '5%',
  },
  middleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  bottomContainer: {
    alignItems: 'center',
    paddingVertical: '5%',
  }
});
