import React from 'react';
import axios from 'axios';
import { AsyncStorage } from 'react-native';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  Modal,
  TouchableHighlight,
} from 'react-native';
import { List, ListItem, Button, SearchBar } from 'react-native-elements';
import { WebBrowser } from 'expo';
import ChartView from 'react-native-highcharts';


export default class RepoScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo',
  };

  constructor(props){
    super(props);

    this.state = {
      data: [],
      bufferData: [],
      pageNum: 1,
      loading: false,
      baseUser:  'cs242HW', //'mdo', //'toxtli',
      query: '',
      modalVisible: false,
      repoName: 'name', // Below are for modal (including this attri)
      numContrs: 30,
      numPulls: 40,
      numCommits: 50,
    };
    this.makeRequest = this.makeRequest.bind(this);
  }

  componentDidMount(){
    this.makeRequest(this.state.baseUser);
  }

  _handlePress = (url) => {
    WebBrowser.openBrowserAsync(url);
  }

  _storeData = async (key, item) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(item));
    } catch (e) {
      console.log(e.message);
    }
  }

  _retrieveData = async (key) => {
    try {
      const data = await AsyncStorage.getItem(key+'Repos');
      console.log("repo key: "+key+'Repos');
      console.log("repo data: "+data);
      if (data){
        this.setState({
          data: JSON.parse(data),
          bufferData: JSON.parse(data),
          loading: false,
        });
      } else {
        axios.get('https://api.github.com/users/'+key+'/repos?page='+this.state.pageNum)
          .then((res) => {
            if (res.length != 0){
              var data = []
              for (var i=0; i<res.data.length; i++){
                const id = res.data[i].id;
                const name = res.data[i].name;
                const owner = res.data[i].owner.login;
                const description = res.data[i].description;
                const url = res.data[i].html_url;
                data.push({id: id, name: name, owner: owner, description: description, url: url})
              }
              this.setState({
                data: data,
                bufferData: data,
                loading: false,
              });
              this._storeData(key+'Repos', data);
            }
          })
          .catch(err => {
            console.log(err);
          })
      }
    } catch (e) {
      console.log(e.message);
    }
  }

  makeRequest = (userName) => {
    this.setState({
      loading: true,
    })

    this._retrieveData(userName);
  }

  _handleQueryChange = query => {
    this.setState(state => ({ ...state, query: query || "" }), () => {
      const query = this.state.query;
      if (query){
        let tempData = [];
        for (var i=0; i<this.state.bufferData.length; i++){
          if (this.state.bufferData[i].name.toLowerCase().includes(query.toLowerCase())){
            tempData.push(this.state.bufferData[i]);
          }
        }
        this.setState({
          data: tempData,
        });
      } else {
        this.setState({
          data: this.state.bufferData,
        });
      }
    });
  }

  _handleSearchClear = () => this._handleQueryChange("");

  _handleSearchCancel = () => this._handleQueryChange("");

  _closeModal = () => {
    this.setState({ modalVisible: false });
  }

  _showVisual = name => {
    // this.setState({
    //   modalVisible: true,
    //   repoName: name,
    // });

    axios.get('https://api.github.com/repos/'+this.state.baseUser+'/'+name+'/stats/contributors')
      .then((res) => {
        if (res.data.length){
          this.setState({
            numContrs: res.data[0].total,
            numCommits: res.data[0].weeks[0].c,
            modalVisible: true,
            repoName: name,
          });
        } else {
          this.setState({
            numContrs: 1,
            numCommits: 1,
            modalVisible: true,
            repoName: name,
          })
        }
      })

    axios.get('https://api.github.com/repos/'+this.state.baseUser+'/'+name+'/pulls')
      .then((res) => {
        const array = res.data;
        if (array.length){
          this.setState({
            numPulls: res.data[0].number,
          });
        } else {
          this.setState({
            numPulls: 0,
          })
        }
      })
  }

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: "#CED0CE",
          marginLeft: "2%",
          marginRight: "2%",
        }}
      />
    );
  };

  render() {
      var config = {
        chart: {
          type: 'pie',
        },
        title: {
          text: this.state.repoName,
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y} ',
              }
          }
       },
       series: [{
          colorByPoint: true,
          data: [{
              name: '# Latest Commits within One Month',
              y: this.state.numCommits,
              sliced: true,
              selected: true
          }, {
              name: '# Unique Contributors',
              y: this.state.numContrs,
          }, {
              name: '# New Pulls within A Week',
              y: this.state.numPulls,
          }]
       }],
     };

    return (
      <View>
        <SearchBar
          lightTheme
          platform='ios'
          placeholder='Search for a repo ...'
          onChangeText={this._handleQueryChange.bind(this)}
          onClear={this._handleSearchClear.bind(this)}
          onCancel={this._handleSearchCancel.bind(this)}
          value={this.state.query}
        />
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem
              title={item.name}
              titleStyle={{fontWeight: 'bold'}}
              underlayColor='#E5E8E8'
              rightTitle={item.owner}
              subtitle={item.description}
              subtitleNumberOfLines={3}
              containerStyle={{ borderBottomWidth: 0 }}
              onPress={this._handlePress.bind(this, item.url)}
              rightIcon = {<Button
                            title='Visual'
                            onPress={this._showVisual.bind(this, item.name)}
                            buttonStyle={{
                              backgroundColor: 'rgba(195, 155, 211, 1)',
                              borderColor: 'transparent',
                              borderWidth: 0,
                              alignSelf: 'center',
                            }}
                            />
                          }
            />
          )}
          keyExtractor={item => item.id.toString()}
          ListFooterComponent={this.renderFooter}
          ItemSeparatorComponent={this.renderSeparator}
        />
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{marginTop: 22}}>
            <View>
              <ChartView style={{height: 600}} config={config}></ChartView>
              <Button
                title='Close'
                onPress={this._closeModal.bind(this)}
                buttonStyle={{
                  backgroundColor: 'rgba(195, 155, 211, 1)',
                  borderColor: 'transparent',
                  borderWidth: 0,
                  alignSelf: 'center',
                }}
              />
            </View>
          </View>
        </Modal>

      </View>
    );
  }
}

// Citations: https://github.com/react-native-training/react-native-elements/issues/1064
