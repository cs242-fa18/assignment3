import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import ProfileScreen from '../screens/Profile/ProfileScreen';
import Detail from '../screens/Profile/Detail';
import Header from '../screens/Profile/Header';
import Intro from '../screens/Profile/Intro';

jest.mock('../screens/Profile/Detail');

beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
  Detail.mockClear();
});

it('We can check if the consumer called the class constructor', () => {
  const profile = new ProfileScreen();
  expect(Detail).not.toHaveBeenCalledTimes(1);
});
